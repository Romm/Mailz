<?php
if (!defined('TYPO3_MODE')) {
    throw new \Exception('Access denied.');
}

/** @noinspection PhpUndefinedVariableInspection */
call_user_func(
    function ($extensionKey) {

        // Registering the events list from TypoScript configuration (when in backend context).
        if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded($extensionKey)
            && TYPO3_MODE == 'BE'
        ) {
            \Mopolo\Mailz\Service\EventRegistry::getInstance()->registerEvents();
        }

    },
    $_EXTKEY
);
