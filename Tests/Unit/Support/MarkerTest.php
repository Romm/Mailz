<?php

namespace Mopolo\Mailz\Tests\Unit\Support;

use Mopolo\Mailz\Support\Tag\Marker;
use Nimut\TestingFramework\TestCase\UnitTestCase;

class MarkerTest extends UnitTestCase
{
    /**
     * @test
     */
    public function hasLabel_returns_false_if_no_label_is_provided()
    {
        $name = 'Foo';

        $marker = new Marker($name);

        $hasLabel = $marker->hasLabel();

        $this->assertFalse($hasLabel);
    }

    /**
     * @test
     */
    public function hasLabel_returns_true_if_a_label_is_provided()
    {
        $name = 'Foo';
        $label = 'Bar';

        $marker = new Marker($name, $label);

        $hasLabel = $marker->hasLabel();

        $this->assertTrue($hasLabel);
    }

    /**
     * @test
     */
    public function can_store_a_value()
    {
        $name = 'Foo';
        $value = 'Some value';

        $marker = new Marker($name);
        $marker->setValue($value);

        $storedValue = $marker->getValue();

        $this->assertEquals($value, $storedValue);
    }

    /**
     * @test
     */
    public function can_format_own_name_with_provided_format()
    {
        $name = 'FooBar';
        $label = 'Lorem ipsum';
        $format = '--%s--';
        $expectedFormattedName = '--FOO_BAR--';

        $marker = new Marker($name, $label, $format);

        $formattedName = $marker->getFormattedName();

        $this->assertEquals($expectedFormattedName, $formattedName);
    }

    /**
     * @test
     */
    public function can_format_own_name_with_default_format()
    {
        $name = 'FooBar';

        /** @var \PHPUnit_Framework_MockObject_MockObject|Marker $marker */
        $marker = $this->getMockBuilder(Marker::class)
            ->setConstructorArgs([$name])
            ->setMethods(['getDefaultFormat'])
            ->getMock();

        $marker->expects($this->once())
            ->method('getDefaultFormat')
            ->willReturn('==%s==');

        $expectedFormattedName = '==FOO_BAR==';

        $formattedName = $marker->getFormattedName();

        $this->assertEquals($expectedFormattedName, $formattedName);
    }
}
