#
# Table structure for table 'tx_mailz_domain_model_notification'
#
CREATE TABLE tx_mailz_domain_model_notification (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	event varchar(255) DEFAULT '' NOT NULL,
	layout varchar(255) DEFAULT '' NOT NULL,
	send_from varchar(255) DEFAULT '' NOT NULL,
	send_from_custom tinyint(4) unsigned DEFAULT '0' NOT NULL,
	send_to_manual varchar(512) DEFAULT '' NOT NULL,
	send_to_provided text NOT NULL,
	send_cc_manual varchar(512) DEFAULT '' NOT NULL,
	send_cc_provided text NOT NULL,
	send_bcc_manual varchar(512) DEFAULT '' NOT NULL,
	send_bcc_provided text NOT NULL,
	mail_subject varchar(255) DEFAULT '' NOT NULL,
	mail_body text NOT NULL,
	signature_custom tinyint(4) unsigned DEFAULT '0' NOT NULL,
	signature text NOT NULL,
-- 	advanced_configuration mediumblob,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)
);
