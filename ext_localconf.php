<?php
if (!defined('TYPO3_MODE')) {
    throw new \Exception('Access denied.');
}

call_user_func(
    function ($extensionKey) {

        \Mopolo\Mailz\Service\TypoScriptService::addConfigurationFilePath('EXT:mailz/Configuration/TypoScript/Default/setup.txt');
        \Mopolo\Mailz\Service\TypoScriptService::addConstantsFilePath('EXT:mailz/Configuration/TypoScript/Default/constants.txt');

        if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][\Mopolo\Mailz\Support\MailzConstants::CACHE_ID])) {
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][\Mopolo\Mailz\Support\MailzConstants::CACHE_ID] = [
                'backend'	=> \TYPO3\CMS\Core\Cache\Backend\FileBackend::class,
                'frontend'	=> \TYPO3\CMS\Core\Cache\Frontend\VariableFrontend::class,
                'groups'	=> ['all', 'system', 'pages'],
            ];
        }

    },
    $GLOBALS['_EXTKEY']
);
