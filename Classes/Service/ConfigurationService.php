<?php

namespace Mopolo\Mailz\Service;

use Mopolo\Mailz\Support\Configuration\Configuration;
use Mopolo\Mailz\Support\MailzConstants;
use Romm\ConfigurationObject\ConfigurationObjectFactory;
use TYPO3\CMS\Core\SingletonInterface;

class ConfigurationService implements SingletonInterface
{
    /**
     * @var CacheService
     */
    private $cacheService;

    /**
     * @var TypoScriptService
     */
    private $typoScriptService;

    /**
     * @param CacheService $cacheService
     * @param TypoScriptService $typoScriptService
     */
    public function __construct(CacheService $cacheService, TypoScriptService $typoScriptService)
    {
        $this->cacheService = $cacheService;
        $this->typoScriptService = $typoScriptService;
    }

    /**
     * todo
     *
     * @return Configuration|null
     */
    public function getConfiguration()
    {
        if (!$this->cacheService->has(MailzConstants::CACHE_KEY_CONFIGURATION)) {
            $arrayConfiguration = $this->typoScriptService->get();

            $objectConfiguration = ConfigurationObjectFactory::convert(
                Configuration::class,
                $arrayConfiguration
            );

            $validationResult = $objectConfiguration->getValidationResult();

            if ($validationResult->hasErrors()) {
                // todo

                return null;
            }

            /** @var Configuration $configuration */
            $configuration = $objectConfiguration->getObject();

            $this->cacheService->set(
                MailzConstants::CACHE_KEY_CONFIGURATION,
                $configuration
            );
        }

        return $this->cacheService->get(MailzConstants::CACHE_KEY_CONFIGURATION);
    }
}
