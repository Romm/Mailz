<?php

namespace Mopolo\Mailz\Service;

use Mopolo\Mailz\Event\Event;
use Mopolo\Mailz\Event\EventRunner;
use Mopolo\Mailz\Exception\EventException;
use Mopolo\Mailz\Support\Configuration\EventGroup;
use Mopolo\Mailz\Support\Configuration\EventGroupItem;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\SignalSlot\Dispatcher;

class EventRegistry implements SingletonInterface
{
    /**
     * @var static
     */
    private static $instance;

    /**
     * @var Dispatcher
     */
    private $signalSlotDispatcher;

    /**
     * @var ConfigurationService
     */
    private $configurationService;

    /**
     * @param Dispatcher $signalSlotDispatcher
     * @param ConfigurationService $configurationService
     */
    public function __construct(Dispatcher $signalSlotDispatcher, ConfigurationService $configurationService)
    {
        $this->signalSlotDispatcher = $signalSlotDispatcher;
        $this->configurationService = $configurationService;
    }

    /**
     * @return \Mopolo\Mailz\Support\Configuration\Configuration|null
     */
    public function getConfiguration()
    {
        return $this->configurationService->getConfiguration();
    }

    /**
     * @return EventGroup[]
     */
    public function getEventGroups()
    {
        return $this->getConfiguration()->getEventGroups();
    }

    /**
     * This method registers all events.
     * It will load all events from either the TypoScript configuration and
     * connect slots to all configured signals.
     *
     * @return $this
     */
    public function registerEvents()
    {
        // Each event group is registered
        foreach ($this->getEventGroups() as $eventGroup) {
            $this->registerEventGroup($eventGroup);
        }

        return $this;
    }

    /**
     * @param $eventKey
     * @return EventGroupItem|null
     */
    public function getEventConfigurationFromKey($eventKey)
    {
        $eventGroups = $this->getEventGroups();

        foreach ($eventGroups as $eventGroup) {
            foreach ($eventGroup->getItems() as $item) {
                if ($item->getKey() === $eventKey) {
                    return $item;
                }
            }
        }

        return null;
    }

    /**
     * @param Event $event
     * @return EventGroupItem|null
     */
    public function getEventConfiguration(Event $event)
    {
        $eventGroups = $this->getEventGroups();

        foreach ($eventGroups as $eventGroup) {
            foreach ($eventGroup->getItems() as $item) {
                if ($item->getEvent() === get_class($event)) {
                    return $item;
                }
            }
        }

        return null;
    }

    /**
     * This methods registers a group/namespace of events
     *
     * @param EventGroup $eventGroup
     * @return $this
     */
    private function registerEventGroup(EventGroup $eventGroup)
    {
        foreach ($eventGroup->getItems() as $eventGroupItem) {
            $this->registerEventGroupItem($eventGroupItem);
        }

        return $this;
    }

    /**
     * This method register one event
     *
     * @param EventGroupItem $item
     * @return $this
     */
    private function registerEventGroupItem(EventGroupItem $item)
    {
        return $this->registerSignal(
            $item->getSignal()->getClassName(),
            $item->getSignal()->getName(),
            $item->getEvent()
        );
    }

    /**
     * Connects a slot to the wanted signal.
     *
     * @param string $signalClassName
     * @param string $signalName
     * @param string $eventClass
     * @return $this
     * @throws EventException
     */
    private function registerSignal($signalClassName, $signalName, $eventClass)
    {
        if (!class_exists($signalClassName)) {
            // todo
        }

        if (!class_exists($eventClass)) {
            throw EventException::doesNotExist($eventClass);
        }

        $this->signalSlotDispatcher->connect(
            $signalClassName,
            $signalName,
            // The slot execution is delegated to a runner
            EventRunner::makeClosure($eventClass)
        );

        return $this;
    }

//    public function registerHook($hookName, $eventClass)
//    {
//        return $this;
//    }

    /**
     * To retrieve the singleton instance.
     *
     * @return static
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            /** @var ObjectManager $objectManager */
            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

            self::$instance = $objectManager->get(static::class);
        }

        return self::$instance;
    }
}
