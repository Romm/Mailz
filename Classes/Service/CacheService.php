<?php

namespace Mopolo\Mailz\Service;

use Mopolo\Mailz\Support\MailzConstants;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\SingletonInterface;

class CacheService implements SingletonInterface
{
    /**
     * @var FrontendInterface
     */
    private $cacheInstance;

    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * @param CacheManager $cacheManager
     */
    public function __construct(CacheManager $cacheManager)
    {
        $this->cacheManager = $cacheManager;
    }

    /**
     * Returns a cache value.
     *
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->getCacheInstance()->get($key);
    }

    /**
     * Stores a value in the cache.
     *
     * @param $key
     * @param $data
     * @param array $tags
     * @return $this
     */
    public function set($key, $data, array $tags = [])
    {
        $this->getCacheInstance()->set($key, $data, $tags);

        return $this;
    }

    /**
     * Checks if an entry exists.
     *
     * @param $key
     * @return bool
     */
    public function has($key)
    {
        return $this->getCacheInstance()->has($key);
    }

    /**
     * Returns the correct cache frontend.
     *
     * @return FrontendInterface
     */
    private function getCacheInstance()
    {
        if (null === $this->cacheInstance
            && $this->cacheManager->hasCache(MailzConstants::CACHE_ID)
        ) {
            $this->cacheInstance = $this->cacheManager->getCache(MailzConstants::CACHE_ID);
        }

        // todo exception if no cache instance

        return $this->cacheInstance;
    }
}
