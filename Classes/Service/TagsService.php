<?php

namespace Mopolo\Mailz\Service;

use Mopolo\Mailz\Event\Event;
use Mopolo\Mailz\Support\Tag\Marker;
use Mopolo\Mailz\Support\Tag\Recipient;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Reflection\ClassReflection;
use TYPO3\CMS\Extbase\Reflection\PropertyReflection;

class TagsService implements SingletonInterface
{
    /**
     * @param $className
     * @return Marker[]
     */
    public function listMarkersFromEventClass($className)
    {
        // todo checker $className de type Event

        return $this->listMarkers($className);
    }

    /**
     * @param Event $event
     * @return Marker[]
     */
    public function listMarkersFromEvent(Event $event)
    {
        return $this->listMarkers($event);
    }

    /**
     * @param $className
     * @return Recipient[]
     */
    public function listRecipientsFromEventClass($className)
    {
        return $this->listRecipients($className);
    }

    /**
     * @param $eventClassNameOrInstance
     * @return Recipient[]
     */
    private function listRecipients($eventClassNameOrInstance)
    {
        if ($eventClassNameOrInstance instanceof Event) {
            $className = get_class($eventClassNameOrInstance);
            $event = $eventClassNameOrInstance;
        } else {
            $className = $eventClassNameOrInstance;
            $event = null;
        }

        $properties = $this->getClassProperties($className);

        $recipients = [];

        // We loop all properties and look for the @recipient annotation
        foreach ($properties as $property) {
            if ($property->isTaggedWith('recipient')) {
                $label = $this->getPropertyLabel($property);

                if (null !== $event) {
                    $value = $property->getValue($event);
                } else {
                    $value = null;
                }

                $recipients[] = new Recipient($property->getName(), $label, $value);
            }
        }

        return $recipients;
    }

    /**
     * @param $eventClassNameOrInstance
     * @return Marker[]
     */
    private function listMarkers($eventClassNameOrInstance)
    {
        if ($eventClassNameOrInstance instanceof Event) {
            $className = get_class($eventClassNameOrInstance);
            $event = $eventClassNameOrInstance;
        } else {
            $className = $eventClassNameOrInstance;
            $event = null;
        }

        $properties = $this->getClassProperties($className);

        $markers = [];

        // We loop all properties and look for the @marker annotation
        foreach ($properties as $property) {
            if ($property->isTaggedWith('marker')) {
                $label = $this->getPropertyLabel($property);

                // todo find a cleaner way to get the marker format
                $marker = new Marker($property->getName(), $label, $className::getMarkerFormat());

                if (null !== $event) {
                    $marker->setValue($property->getValue($event));
                }

                $markers[] = $marker;
            }
        }

        return $markers;
    }

    /**
     * @param string $className
     * @return PropertyReflection[]
     */
    private function getClassProperties($className)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        /** @var ClassReflection $reflection */
        $reflection = $objectManager->get(ClassReflection::class, $className);

        return $reflection->getProperties();
    }

    /**
     * @param PropertyReflection $property
     * @return string
     */
    private function getPropertyLabel(PropertyReflection $property)
    {
        // The label is either another annotation or the property name
        $label = $property->isTaggedWith('label')
            ? $property->getTagValues('label')[0] // todo handle LLL string
            : $property->getName();

        return $label;
    }
}
