<?php

namespace Mopolo\Mailz\Service;

use Mopolo\Mailz\Support\MailzConstants;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Service\TypoScriptService as ExtbaseTypoScriptService;

class TypoScriptService implements SingletonInterface
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var ConfigurationManager
     */
    private $configurationManager;

    /**
     * This class might appear as not existing in the IDE when using TYPO3 v8.
     * It is because it's an alias for \TYPO3\CMS\Core\TypoScript\TypoScriptService
     *
     * @see https://github.com/TYPO3/TYPO3.CMS/blob/master/typo3/sysext/extbase/Migrations/Code/ClassAliasMap.php
     *
     * @var ExtbaseTypoScriptService
     */
    private $typoScriptService;

    /**
     * @param ConfigurationManager $configurationManager
     * @param ExtbaseTypoScriptService $typoScriptService
     */
    public function __construct(ConfigurationManager $configurationManager, ExtbaseTypoScriptService $typoScriptService)
    {
        $this->configurationManager = $configurationManager;
        $this->typoScriptService = $typoScriptService;
    }

    /**
     * To retrieve a configuration key.
     * If no key is given, it returns the entire configuration array.
     *
     * @param string $key
     * @return mixed|null
     */
    public function get($key = null)
    {
        $path = MailzConstants::CONFIGURATION_ROOT_PATH;

        if (null !== $key) {
            $path .= '.' . $key;
        }

        if (!ArrayUtility::isValidPath($this->getConfig(), $path, '.')) {
            return null;
        }

        return ArrayUtility::getValueByPath($this->getConfig(), $path, '.');
    }

    /**
     * Loads the TypoScript configuration and returns it as an array.
     *
     * @return array
     */
    private function getConfig()
    {
        if (null === $this->config) {
            $config = $this->configurationManager->getConfiguration(
                ConfigurationManager::CONFIGURATION_TYPE_FULL_TYPOSCRIPT
            );

            $this->config = $this->typoScriptService->convertTypoScriptArrayToPlainArray($config);
        }

        return $this->config;
    }

    /**
     * todo
     *
     * @param string|array $path The path to the file. Can be an array of paths too.
     */
    public static function addConfigurationFilePath($path)
    {
        ExtensionManagementUtility::addTypoScriptSetup(sprintf('<INCLUDE_TYPOSCRIPT: source="FILE:%s">', $path));
    }

    /**
     * todo
     *
     * @param string|array $path The path to the file. Can be an array of paths too.
     */
    public static function addConstantsFilePath($path)
    {
        ExtensionManagementUtility::addTypoScriptConstants(sprintf('<INCLUDE_TYPOSCRIPT: source="FILE:%s">', $path));
    }
}
