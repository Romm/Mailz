<?php

namespace Mopolo\Mailz\Domain\Event\Extension\TYPO3;

use Mopolo\Mailz\Event\Event;
use TYPO3\CMS\Extensionmanager\Utility\ListUtility;

class ExtensionInstalledEvent extends Event
{
    /**
     * @marker
     * @label Test
     * @var string
     */
    protected $extensionName;

    /**
     * @recipient
     * @label Un utilisateur
     * @var string
     */
    protected $user;

    /**
     * @var ListUtility
     */
    protected $listUtility = null;

    /**
     * @param ListUtility $listUtility
     */
    public function __construct(ListUtility $listUtility)
    {
        $this->listUtility = $listUtility;
    }

    public function run($extensionKey)
    {
        $extension = $this->listUtility->getExtension($extensionKey);

        $this->extensionName = 'dudule';

    }
}
