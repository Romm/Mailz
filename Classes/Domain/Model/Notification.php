<?php

namespace Mopolo\Mailz\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class Notification extends AbstractEntity
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $event;

    /**
     * @var string
     */
    protected $layout;

    /**
     * @var string
     */
    protected $sendFrom;

    /**
     * @var bool
     */
    protected $sendFromCustom;

    /**
     * @var string
     */
    protected $sendToManual;

    /**
     * @var string
     */
    protected $sendToProvided;

    /**
     * @var string
     */
    protected $sendCcManual;

    /**
     * @var string
     */
    protected $sendCcProvided;

    /**
     * @var string
     */
    protected $sendBccManual;

    /**
     * @var string
     */
    protected $sendBccProvided;

    /**
     * @var string
     */
    protected $mailSubject;

    /**
     * @var string
     */
    protected $mailBody;

    /**
     * @var bool
     */
    protected $signatureCustom;

    /**
     * @var string
     */
    protected $signature;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param string $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param string $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * @return string
     */
    public function getSendFrom()
    {
        return $this->sendFrom;
    }

    /**
     * @param string $sendFrom
     */
    public function setSendFrom($sendFrom)
    {
        $this->sendFrom = $sendFrom;
    }

    /**
     * @return bool
     */
    public function isSendFromCustom()
    {
        return $this->sendFromCustom;
    }

    /**
     * @param bool $sendFromCustom
     */
    public function setSendFromCustom($sendFromCustom)
    {
        $this->sendFromCustom = $sendFromCustom;
    }

    /**
     * @return string
     */
    public function getSendToManual()
    {
        return $this->sendToManual;
    }

    /**
     * @param string $sendToManual
     */
    public function setSendToManual($sendToManual)
    {
        $this->sendToManual = $sendToManual;
    }

    /**
     * @return string
     */
    public function getSendToProvided()
    {
        return $this->sendToProvided;
    }

    /**
     * @param string $sendToProvided
     */
    public function setSendToProvided($sendToProvided)
    {
        $this->sendToProvided = $sendToProvided;
    }

    /**
     * @return string
     */
    public function getSendCcManual()
    {
        return $this->sendCcManual;
    }

    /**
     * @param string $sendCcManual
     */
    public function setSendCcManual($sendCcManual)
    {
        $this->sendCcManual = $sendCcManual;
    }

    /**
     * @return string
     */
    public function getSendCcProvided()
    {
        return $this->sendCcProvided;
    }

    /**
     * @param string $sendCcProvided
     */
    public function setSendCcProvided($sendCcProvided)
    {
        $this->sendCcProvided = $sendCcProvided;
    }

    /**
     * @return string
     */
    public function getSendBccManual()
    {
        return $this->sendBccManual;
    }

    /**
     * @param string $sendBccManual
     */
    public function setSendBccManual($sendBccManual)
    {
        $this->sendBccManual = $sendBccManual;
    }

    /**
     * @return string
     */
    public function getSendBccProvided()
    {
        return $this->sendBccProvided;
    }

    /**
     * @param string $sendBccProvided
     */
    public function setSendBccProvided($sendBccProvided)
    {
        $this->sendBccProvided = $sendBccProvided;
    }

    /**
     * @return string
     */
    public function getMailSubject()
    {
        return $this->mailSubject;
    }

    /**
     * @param string $mailSubject
     */
    public function setMailSubject($mailSubject)
    {
        $this->mailSubject = $mailSubject;
    }

    /**
     * @return string
     */
    public function getMailBody()
    {
        return $this->mailBody;
    }

    /**
     * @param string $mailBody
     */
    public function setMailBody($mailBody)
    {
        $this->mailBody = $mailBody;
    }

    /**
     * @return bool
     */
    public function isSignatureCustom()
    {
        return $this->signatureCustom;
    }

    /**
     * @param bool $signatureCustom
     */
    public function setSignatureCustom($signatureCustom)
    {
        $this->signatureCustom = $signatureCustom;
    }

    /**
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @param string $signature
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
    }
}
