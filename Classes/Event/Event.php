<?php

namespace Mopolo\Mailz\Event;

use Mopolo\Mailz\Support\MailzConstants;

abstract class Event
{
    /**
     * This format is used to display the marker in the edit form
     * and when it is replaced in the email subject and body.
     *
     * You can override this value in each event to customize it.
     * You have to put %s in the format, which will be replace by the marker's name.
     *
     * If you want to use the % character, you need to escape it by writing it twice.
     *
     * @var string
     */
    protected static $markerFormat = MailzConstants::DEFAULT_MARKER_FORMAT;

    /**
     * Returns the marker format for the current event class.
     *
     * @return string
     */
    final public static function getMarkerFormat()
    {
        return empty(trim(static::$markerFormat))
            ? MailzConstants::DEFAULT_MARKER_FORMAT
            : static::$markerFormat;
    }
}
