<?php

namespace Mopolo\Mailz\Event;

use Mopolo\Mailz\Factory\EventFactory;
use Mopolo\Mailz\Service\TagsService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class EventRunner
{
    /**
     * @var EventFactory
     */
    private $eventFactory;

    /**
     * @var TagsService
     */
    private $tagsService;

    /**
     * @param EventFactory $eventFactory
     * @param TagsService $tagsService
     */
    public function __construct(EventFactory $eventFactory, TagsService $tagsService)
    {
        $this->eventFactory = $eventFactory;
        $this->tagsService = $tagsService;
    }

    /**
     * Returns a closure to run a specific event.
     *
     * @param $eventClass
     * @return \Closure
     */
    public static function makeClosure($eventClass)
    {
        return function (...$arguments) use ($eventClass) {
            /** @var ObjectManager $objectManager */
            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

            /** @var static $runner */
            $runner = $objectManager->get(static::class);

            $runner->run($eventClass, $arguments);

            // todo faire quelque chose du retour (emmettre un signal ? (qui ne sera pas écoutable pour éviter les loop))
        };
    }

    /**
     * todo
     *
     * @param string $eventClass
     * @param array $arguments
     * @return bool
     */
    private function run($eventClass, array $arguments)
    {
        // We remove the last element (the signal class->method signature)
        // This avoids calling the event's run method with too many arguments
        array_pop($arguments);

        $event = $this->eventFactory->createFromClass($eventClass);

        $result = false;

        if (method_exists($event, 'run')
            && is_callable([$event, 'run'])
        ) {
            $result = $event->run(...$arguments);

            $markers = $this->tagsService->listMarkersFromEvent($event);

            // todo envoyer le mail
        } else {
            // todo faire quelque chose ?
        }

        return $result;
    }
}
