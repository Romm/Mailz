<?php

namespace Mopolo\Mailz\Exception;

class EventException extends MailzException
{
    public static function doesNotExist($eventClass)
    {
        return new static("The event class {$eventClass} does not exist");
    }
}
