<?php

namespace Mopolo\Mailz\Factory;

use Mopolo\Mailz\Event\Event;
use Mopolo\Mailz\Service\EventRegistry;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class EventFactory implements SingletonInterface
{
    /**
     * @var EventRegistry
     */
    private $eventRegistry;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @param EventRegistry $eventRegistry
     * @param ObjectManager $objectManager
     */
    public function __construct(EventRegistry $eventRegistry, ObjectManager $objectManager)
    {
        $this->eventRegistry = $eventRegistry;
        $this->objectManager = $objectManager;
    }

    /**
     * @param $className
     * @return Event
     */
    public function createFromClass($className)
    {
        /** @var Event $event */
        $event = $this->objectManager->get($className);

        // todo exception when not found

        return $event;
    }

    /**
     * @param $name
     * @return Event
     */
    public function createFromName($name)
    {
        $eventConfiguration = $this->eventRegistry->getEventConfigurationFromKey($name);

        return $this->createFromClass($eventConfiguration->getEvent());
    }
}
