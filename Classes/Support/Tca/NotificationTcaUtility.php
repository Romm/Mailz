<?php

namespace Mopolo\Mailz\Support\Tca;

use Mopolo\Mailz\Service\EventRegistry;
use Mopolo\Mailz\Service\TagsService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;

class NotificationTcaUtility
{
    /**
     * Loads all available events and stores them as an array to be used in the TCA.
     *
     * @param $parameters
     */
    public function getEventsList(&$parameters)
    {
        $eventGroups = EventRegistry::getInstance()->getEventGroups();

        foreach ($eventGroups as $eventGroup) {
            // Each group has a label
            $parameters['items'][] = [
                $eventGroup->getLabel(),
                '--div--',
            ];

            foreach ($eventGroup->getItems() as $item) {
                $parameters['items'][] = [
                    $item->getLabel(),
                    $item->getKey(),
                ];
            }
        }
    }

    /**
     * Loads all markers for the current selected event and
     * formats them as a list to be displayed on the edit form.
     *
     * @param $parameters
     * @return string
     */
    public function getMarkersLabel(&$parameters)
    {
        $eventClass = $this->getCurrentEventClass($parameters);

        /** @var TagsService $tagsService */
        $tagsService = GeneralUtility::makeInstance(TagsService::class);

        $markers = $tagsService->listMarkersFromEventClass($eventClass);

        $output = '';

        foreach ($markers as $marker) {
            $text = '<strong>' . $marker->getFormattedName() . '</strong>';

            if ($marker->hasLabel()) {
                $text .= ': ' . $marker->getLabel();
            }

            $output .= '<li>' . $text . '</li>';
        }

        $output = '<ul>' . $output . '</ul>';

        return $output;
    }

    /**
     * Loads all recipients provided by the selected event
     * and stores them as an array to be used in the TCA.
     *
     * @param $parameters
     */
    public function getRecipientsList(&$parameters)
    {
        $eventClass = $this->getCurrentEventClass($parameters);

        /** @var TagsService $tagsService */
        $tagsService = GeneralUtility::makeInstance(TagsService::class);

        $recipients = $tagsService->listRecipientsFromEventClass($eventClass);

        foreach ($recipients as $recipient) {
            $parameters['items'][] = [
                $recipient->getLabel(),
                $recipient->getName(),
            ];
        }
    }

    public function getDefaultSignature($parameters)
    {
        return 'Hello';
    }

    public function getDefaultExpediter($parameters)
    {
        return 'foo@bar.org';
    }

    /**
     * @param $parameters
     * @return string
     */
    protected function getCurrentEventClass($parameters)
    {
        // We check if the record already exists in the database
        if (MathUtility::canBeInterpretedAsInteger($parameters['row']['uid'])) {
            $eventKey = $parameters['row']['event'][0];
        } else {
            // If not, we use the first configured event as the current one
            $firstEventGroup = EventRegistry::getInstance()
                ->getConfiguration()
                ->getFirstEventGroup();

            $eventKey = $firstEventGroup->getFirstItem()->getKey();
        }

        return EventRegistry::getInstance()
            ->getEventConfigurationFromKey($eventKey)
            ->getEvent();
    }
}
