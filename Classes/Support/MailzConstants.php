<?php

namespace Mopolo\Mailz\Support;

/**
 * This class is a collection of constants specific to this extension
 */
final class MailzConstants
{
    /**
     * The extension key
     */
    const EXTENSION_KEY = 'mailz';

    /**
     * Used to retrieve the FrontendCache instance
     */
    const CACHE_ID = 'mailz';

    /**
     * This key is used by the cache
     */
    const CACHE_KEY_CONFIGURATION = 'config';

    /**
     * Root node for all configurations
     */
    const CONFIGURATION_ROOT_PATH = 'config.tx_' . self::EXTENSION_KEY;

    /**
     * The default format for event markers
     *
     * @see \Mopolo\Mailz\Event\Event
     */
    const DEFAULT_MARKER_FORMAT = '#%s#';
}
