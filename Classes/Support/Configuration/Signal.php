<?php

namespace Mopolo\Mailz\Support\Configuration;

use Romm\ConfigurationObject\Traits\ConfigurationObject\MagicMethodsTrait;

class Signal
{
    use MagicMethodsTrait;

    /**
     * @var string
     */
    protected $className;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
