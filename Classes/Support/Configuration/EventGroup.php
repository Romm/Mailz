<?php

namespace Mopolo\Mailz\Support\Configuration;

use Romm\ConfigurationObject\Traits\ConfigurationObject\MagicMethodsTrait;

class EventGroup
{
    use MagicMethodsTrait;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var \Mopolo\Mailz\Support\Configuration\EventGroupItem[]
     */
    protected $items;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return EventGroupItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return EventGroupItem|null
     */
    public function getFirstItem()
    {
        $items = $this->getItems();

        if (!is_array($items)) {
            return null;
        }

        /** @var EventGroupItem $first */
        $first = array_pop(array_reverse($items));

        return $first;
    }
}
