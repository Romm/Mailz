<?php

namespace Mopolo\Mailz\Support\Configuration;

use Mopolo\Mailz\Support\MailzConstants;
use Romm\ConfigurationObject\ConfigurationObjectInterface;
use Romm\ConfigurationObject\Service\Items\Cache\CacheService;
use Romm\ConfigurationObject\Service\ServiceFactory;
use Romm\ConfigurationObject\Traits\ConfigurationObject\MagicMethodsTrait;

class Configuration implements ConfigurationObjectInterface
{
    use MagicMethodsTrait;

    /**
     * @var \Mopolo\Mailz\Support\Configuration\EventGroup[]
     * @validate NotEmpty
     */
    protected $events;

    /**
     * @return EventGroup[]
     */
    public function getEventGroups()
    {
        return $this->events;
    }

    /**
     * @return EventGroup
     */
    public function getFirstEventGroup()
    {
        $eventGroups = $this->getEventGroups();

        /** @var EventGroup $firstEventGroup */
        $firstEventGroup = array_pop(array_reverse($eventGroups));

        return $firstEventGroup;
    }

    /**
     * @inheritdoc
     */
    public static function getConfigurationObjectServices()
    {
        return ServiceFactory::getInstance()
            ->attach(CacheService::class)
            ->setOption(CacheService::OPTION_CACHE_NAME, MailzConstants::CACHE_KEY_CONFIGURATION);
    }
}
