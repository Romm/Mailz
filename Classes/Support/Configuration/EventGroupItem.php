<?php

namespace Mopolo\Mailz\Support\Configuration;

use Romm\ConfigurationObject\Traits\ConfigurationObject\MagicMethodsTrait;
use Romm\ConfigurationObject\Traits\ConfigurationObject\StoreArrayIndexTrait;

class EventGroupItem
{
    use MagicMethodsTrait;
    use StoreArrayIndexTrait;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var string
     */
    protected $event;

    /**
     * @var \Mopolo\Mailz\Support\Configuration\Signal
     */
    protected $signal;

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->getArrayIndex();
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @return Signal
     */
    public function getSignal()
    {
        return $this->signal;
    }
}
