<?php

namespace Mopolo\Mailz\Support\Tag;

use Mopolo\Mailz\Event\Event;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * This class represent a single marker annotation in the Event class
 *
 * @see \Mopolo\Mailz\Event\Event
 */
class Marker
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $value;

    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $format;

    /**
     * @param string $name
     * @param string $label
     * @param string $format
     */
    public function __construct($name, $label = null, $format = null)
    {
        $this->name = $name;
        $this->label = $label;
        $this->format = $format;
    }

    /**
     * @return bool
     */
    public function hasLabel()
    {
        return !empty(trim($this->getLabel()));
    }

    /**
     * Returns the name of the marker based on the provided format and in uppercase.
     *
     * For example, with the format #%s#, 'fooBar' becomes '#FOO_BAR#'
     *
     * @return string
     */
    public function getFormattedName()
    {
        return sprintf(
            $this->getFormat(),
            strtoupper(
                GeneralUtility::camelCaseToLowerCaseUnderscored($this->getName())
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    protected function getFormat()
    {
        if (null === $this->format) {
            // If not format is provided, we retrieve the default one
            $this->format = $this->getDefaultFormat();
        }

        return $this->format;
    }

    /**
     * @return string
     */
    protected function getDefaultFormat()
    {
        return Event::getMarkerFormat();
    }
}
