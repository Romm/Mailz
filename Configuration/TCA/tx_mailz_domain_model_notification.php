<?php
defined('TYPO3_MODE') or die();

return [
    'ctrl' => [
        'title' => 'Notif',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,

        'requestUpdate' => 'event,signature_custom,send_from_custom',

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title',
        'iconfile' => 'EXT:mailz/Resources/Public/Icons/notification.gif'
    ],

    'palettes' => [
        'mail' => [
            'showitem' => 'mail_subject,markers,--linebreak--,mail_body,--linebreak--,signature_custom,--linebreak--,signature,signature_default',
            'canNotCollapse' => true
        ],
        'send_to' => [
            'showitem' => 'send_to_manual,--linebreak--,send_to_provided',
            'canNotCollapse' => true,
        ],
        'send_cc' => [
            'showitem' => 'send_cc_manual,--linebreak--,send_cc_provided',
            'canNotCollapse' => true,
        ],
        'send_bcc' => [
            'showitem' => 'send_bcc_manual,--linebreak--,send_bcc_provided',
            'canNotCollapse' => true,
        ],
    ],

    'types' => [
        '0' => [
            'showitem' => '
				title, sys_language_uid, hidden,
				--div--;LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.tab.mail_configuration,
                    event, layout,
                    --palette--;LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.palette.mail;mail,
                --div--;LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.tab.mail_recipients,
                    --palette--;LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.palette.mail_recipients_to;send_to,
                    --palette--;LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.palette.mail_recipients_cc;send_cc,
                    --palette--;LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.palette.mail_recipients_bcc;send_bcc,
                --div--;LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.tab.mail_expediter,
                    send_from_custom,--linebreak--,send_from,send_from_default
				'
        ]
    ],

    'columns' => [

        // General

        'title' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ]
        ],

        // Mail configuration

        'layout' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.layout',
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
            'config' => [
                'type' => 'select',
//                'itemsProcFunc' => 'Romm\\SwiftNotifier\\Utility\\NotificationTcaUtility->getLayoutListOptions',
                'items' => [
                    ['Todo', 'todo'],
                    ['Bar', 'bar'],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => 'required'
            ]
        ],

        'event' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.event',
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'itemsProcFunc' => \Mopolo\Mailz\Support\Tca\NotificationTcaUtility::class . '->getEventsList',
            ]
        ],

        // Mail content

        'mail_subject' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.mail_subject',
            'config' => [
                'type' => 'input',
                'size' => 40,
                'eval' => 'trim,required'
            ]
        ],

        'mail_body' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.mail_body',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15
            ]
        ],

        'markers' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.markers',
            'l10n_display' => 'defaultAsReadonly',
            'config' => [
                'type' => 'user',
                'userFunc' => \Mopolo\Mailz\Support\Tca\NotificationTcaUtility::class . '->getMarkersLabel',
            ]
        ],

        // Expediter

        'send_from_custom' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.send_from_custom',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ],
        ],

        'send_from_default' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.send_from_default',
            'displayCond' => 'FIELD:send_from_custom:=:0',
            'config' => [
                'type' => 'user',
                'userFunc' => \Mopolo\Mailz\Support\Tca\NotificationTcaUtility::class . '->getDefaultExpediter',
            ],
        ],

        'send_from' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.send_from',
            'displayCond' => 'FIELD:send_from_custom:=:1',
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'email,required',
                'default' => '',
                'placeholder' => 'no-reply@example.org',
            ]
        ],

        // Recipients

        'send_to_manual' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.send_to',
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
            'config' => [
                'type' => 'input',
                'size' => 512,
                'eval' => 'trim',
                'placeholder' => 'john@example.com,jane@example.org'
            ]
        ],

        'send_to_provided' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.send_to_provided',
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
            'config' => [
                'type' => 'select',
                'itemsProcFunc' => \Mopolo\Mailz\Support\Tca\NotificationTcaUtility::class . '->getRecipientsList',
//                'renderType' => 'selectMultiple',
                'size' => 5,
                'maxitems' => 128,
            ]
        ],

        'send_cc_manual' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.send_cc',
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
            'config' => [
                'type' => 'input',
                'size' => 512,
                'eval' => 'trim',
                'placeholder' => 'john@example.com,jane@example.org'
            ]
        ],

        'send_cc_provided' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.send_cc_provided',
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
            'config' => [
                'type' => 'select',
                'itemsProcFunc' => \Mopolo\Mailz\Support\Tca\NotificationTcaUtility::class . '->getRecipientsList',
//                'renderType' => 'selectMultiple',
                'size' => 5,
                'maxitems' => 128,
            ]
        ],

        'send_bcc_manual' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.send_bcc',
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
            'config' => [
                'type' => 'input',
                'size' => 512,
                'eval' => 'trim',
                'placeholder' => 'john@example.com,jane@example.org'
            ]
        ],

        'send_bcc_provided' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.send_bcc_provided',
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
            'config' => [
                'type' => 'select',
                'itemsProcFunc' => \Mopolo\Mailz\Support\Tca\NotificationTcaUtility::class . '->getRecipientsList',
//                'renderType' => 'selectMultiple',
                'size' => 5,
                'maxitems' => 128,
            ]
        ],

        // Signature

        'signature_custom' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.signature_custom',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ],
        ],

        'signature_default' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.signature_default',
            'displayCond' => 'FIELD:signature_custom:=:0',
            'config' => [
                'type' => 'user',
                'userFunc' => \Mopolo\Mailz\Support\Tca\NotificationTcaUtility::class . '->getDefaultSignature',
            ],
        ],

        'signature' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:mailz/Resources/Private/Language/locallang.xlf:notification.signature',
            'displayCond' => 'FIELD:signature_custom:=:1',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 8,
                'default' => '',
            ]
        ],

        // Default TYPO3 columns

        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => [
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1],
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0]
                ]
            ]
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'l10n_display' => 'defaultAsReadonly',
            'config' => [
                'type' => 'select',
                'foreign_table' => 'tx_mailz_domain_model_notification',
                'foreign_table_where' => 'AND tx_mailz_domain_model_notification.pid=###CURRENT_PID### AND tx_mailz_domain_model_notification.sys_language_uid IN (-1,0)',
                'items' => [
                    ['', 0]
                ]
            ]
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255
            ]
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check'
            ]
        ],
        'starttime' => [
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ]
            ]
        ],
        'endtime' => [
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ]
            ]
        ],

    ],
];
