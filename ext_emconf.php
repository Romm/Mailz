<?php
/** @noinspection PhpUndefinedVariableInspection */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Mailz',
    'version' => '0.1.0',
    'state' => 'experimental',
    'description' => 'Todo',
    'author' => 'Nathan Boiron',
    'author_email' => 'nathan.boiron@gmail.com',
    'category' => 'backend',
    'clearCacheOnLoad' => 1,
    'constraints' => [
        'depends' => [
            'typo3' => '6.2.0-8.7.99',
        ]
    ]
];
